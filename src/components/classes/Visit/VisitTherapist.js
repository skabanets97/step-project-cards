import { Visit } from "./Visit.js";

export class VisitTherapist extends Visit {
    constructor({fullName, visitPurpose, priority, selectDoctor, description, status, id, age}) {
        super({fullName, visitPurpose, priority, selectDoctor, description, status, id});
        this.age = age;
    }

    render(parent) {
        super.render(parent);

        this.addRow = this.card.querySelector("#additional-row")
        this.addRow.insertAdjacentHTML("beforeend", `Age: ${this.age}`)
        this.card.classList.add("card-therapist");
        parent.append(this.card);
    }
}