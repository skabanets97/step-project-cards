import { deleteCard } from "../../functions/deleteCard.js";

export class Visit {
    constructor({fullName, visitPurpose, priority, selectDoctor, description, status, id}) {
        this.fullName = fullName;
        this.visitPurpose = visitPurpose;
        this.priority = priority;
        this.selectDoctor = selectDoctor;
        this.description = description;
        this.status = status;
        this.id = id;
        this.card = document.createElement("div");
        this.card.classList.add("card-template");
    }

    render(parent) {
        this.card.insertAdjacentHTML('beforeend', `
        <div id="card-action" class="d-flex align-items-center justify-content-between card-action${this.status}">
            <button type="button" class="btn btn${this.status} btn-status" data-card="${this.id}" data-status="${this.status}"><span id="statusDone">${this.status}</span></button>
            <div class="d-flex align-items-center gap-3">
                <button type="button" class="btn edit-visit-btn" id="editBtn" data-card="${this.id}">Edit Card</button>
                <button type="button" class="deleteBtn btn-close" aria-label="Close" id="deleteBtn" data-cardID="${this.id}" ></button>
            </div>
        </div>
        <div class="card-body">
            <h5 class="card-title text-center mb-3">${this.fullName}</h5>
            <div class="d-flex justify-content-between">
            <h6 class="card-subtitle mb-2">Doctor: ${this.selectDoctor}</h6>
            </div>
            <div class="accordion" id="accordionFlush">
                <button id="showMore" class="accordion-button collapsed show-more-btn" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-${this.id}" aria-expanded="false" aria-controls="flush-collapseOne">
                    Show more
                </button>
                <div id="collapse-${this.id}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <ul class="card-list list-group list-group-flush">
                        <li class="card-list-item list-group-item">Priority: ${this.priority}</li>
                        <li class="card-list-item list-group-item">Purpose: ${this.visitPurpose}</li>
                        ${this.description ? `<li class="card-list-item list-group-item">Description: ${this.description}</li>` : ''}
                        <li id="additional-row" class="card-list-item list-group-item"></li>
                    </ul>
                </div>
             </div>
        </div>
        `)

        parent.append(this.card)

        this.card.addEventListener("click", async(e) =>{
                const value = e.target;
            if (value.classList.contains("deleteBtn")) {
                e.preventDefault();
                let cardID = Number(value.getAttribute("data-cardID"));
                await deleteCard(cardID);
            }

        })

        if(this.status === "Done") {
            this.card.classList.add('done-visit');
        }
    }
}