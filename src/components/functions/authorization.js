import { loginButton } from "../variables/variables.js";
import { ModalLogin } from "../classes/Modal/ModalLogin.js";
import { getToken } from "../api/httpRequests.js";
import { setMainPage } from "./Main-page/setMainPage.js";
import { getAllVisitCards } from "./getAllVisitCards";

export const authorization = () => {

    loginButton.addEventListener('click', e => {
        const modalLogin = new ModalLogin ();
        modalLogin.render();

        const authorizationButton = document.querySelector('#authorization');

        authorizationButton.addEventListener('click', e => {
            e.preventDefault();

            const email  = document.querySelector('#user-email').value;
            const password = document.querySelector('#user-password').value;
            const authorizationError = document.querySelector('.authorization-error-massage');

            if(email && password) {
                ( async () => {
                    try {
                        await getToken({email, password})
                        setMainPage();
                        modalLogin.close();
                        await getAllVisitCards();
                    } catch (error) {
                        authorizationError.innerHTML = 'There is no such user! You may have entered the wrong email or password.';
                    }

                }) ()
            } else {
                authorizationError.innerHTML = 'All fields must be filled!';
            }

        })

    })

}