import { ModalCreateVisit } from "../classes/Modal/ModalCreateVisit.js";
import { formToObject } from "./formToObject.js";
import { createCard } from "../api/httpRequests.js";
import { getAllVisitCards } from "./getAllVisitCards.js";

export function createVisit () {

    const createModal = document.querySelector("#create-card");

    createModal.addEventListener("click", e => {
        const modalCreateVisit = new ModalCreateVisit();
        modalCreateVisit.render();

        const form = document.querySelector(".form");
        const createBtn = form.querySelector(".submit");
        createBtn.addEventListener("click", async (e) => {
            e.preventDefault();

            const formData = new FormData(form);
            const visitDetails = formToObject(formData)
            visitDetails.status = 'Open';
            modalCreateVisit.close();

            await createCard(visitDetails);
            await getAllVisitCards();

        })
    });
}