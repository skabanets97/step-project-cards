import { logoutButton } from "../variables/variables.js";
import { setMainPage } from "./Main-page/setMainPage.js";

export const logout =  () => {

    logoutButton.addEventListener('click', async () => {
        localStorage.clear();
        setMainPage();
    })

}