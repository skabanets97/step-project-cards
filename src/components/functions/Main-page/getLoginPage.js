import { loginButton, createVisitButton, logoutButton, visitCardsStorage, filterWrapper } from "../../variables/variables.js";

export const getLoginPage = () => {
    loginButton.classList.remove('hide');
    logoutButton.classList.add('hide');
    createVisitButton.classList.add('hide');
    filterWrapper.classList.add('hide');
    visitCardsStorage.innerHTML = '<div class="main-image">';
}