import { getHomePage } from "./getHomePage.js"
import { getLoginPage } from "./getLoginPage.js";
import { getAllVisitCards } from "../getAllVisitCards.js";

export const setMainPage = () => {
   const isAuth = window.localStorage.getItem('token');

    if(isAuth) {
        getHomePage();
        (async () => await getAllVisitCards()) ();
    } else {
        getLoginPage();
    }
}