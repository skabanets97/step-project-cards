import { filter, filterPriority, filterSearch, filterStatus} from "../../variables/variables.js";
import { renderVisitCards } from "../renderVisitCards.js";

export const setFilterStatus = (arrayVisitCards) => {

    filterStatus.addEventListener('change', e => {

        if( filterPriority.value !== 'all' ||  filterSearch.value !== '') {
            filter.reset();
        }

        const value = e.target.value;

        if(value === 'all'){
            renderVisitCards(arrayVisitCards);
        }

        if(value === 'open'){
            let filteredArrayOpenStatus = [];
            arrayVisitCards.forEach( card => {
                if(card.status === 'Open'){
                    filteredArrayOpenStatus.push(card);
                }
            })
            renderVisitCards(filteredArrayOpenStatus);
        }

        if(value === 'done'){
            let filteredArrayDoneStatus = [];
            arrayVisitCards.forEach( card => {
                if(card.status === 'Done'){
                    filteredArrayDoneStatus.push(card);
                }
            })
            renderVisitCards(filteredArrayDoneStatus);
        }

    });
}