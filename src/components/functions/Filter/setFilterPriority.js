import { filter, filterPriority, filterSearch, filterStatus } from "../../variables/variables.js";
import { renderVisitCards } from "../renderVisitCards.js";

export const setFilterPriority = (arrayVisitCards) => {

    filterPriority.addEventListener('change', e => {

        if( filterStatus.value !== 'all' ||  filterSearch.value !== "") {
            filter.reset();
        }

        const value = e.target.value;

        if(value === 'all'){
            renderVisitCards(arrayVisitCards);
        }

        if(value === 'high'){
            let filteredArrayHighStatus = [];
            arrayVisitCards.forEach( card => {
                if(card.priority === 'High'){
                    filteredArrayHighStatus.push(card);
                }
            })
            renderVisitCards(filteredArrayHighStatus);
        }

        if(value === 'normal'){
            let filteredArrayNormalPriority = [];
            arrayVisitCards.forEach( card => {
                if(card.priority === 'Normal'){
                    filteredArrayNormalPriority.push(card);
                }
            })
            renderVisitCards(filteredArrayNormalPriority);
        }

        if(value === 'low'){
            let filteredArrayLowPriority = [];
            arrayVisitCards.forEach( card => {
                if(card.priority === 'Low'){
                    filteredArrayLowPriority.push(card);
                }
            })
            renderVisitCards(filteredArrayLowPriority);
        }

    });
}

