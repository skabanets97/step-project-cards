import { getAllCards } from "../api/httpRequests.js";
import { renderVisitCards } from "./renderVisitCards.js";
import { setFilter } from "./Filter/setFilter.js";

export const getAllVisitCards = async () => {
    await getAllCards();
    let allVisitCards = JSON.parse(localStorage.getItem('allVisitCards'));
    renderVisitCards(allVisitCards);
    setFilter(allVisitCards);
}
